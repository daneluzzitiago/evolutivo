/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BlackJack;

import Baralho.Baralho;
import Baralho.Carta;
import java.util.ArrayList;

/**
 *  Controlador de operações do jogo BlackJack
 *  @author tldan daneluzzitiago@gmail.com
 */
public class BJControlador {
    private ArrayList<BJJogador> jogadores;         //Lista de jogadores ativos
    private int maxJogadores = 8;                   //Limitador do máximo de jogadores
    private boolean bitsControle[] ;                //Controle das ações já realizadas no jogo
                                                    //Verificar zeraBitsControle()
    private Baralho baralho;                        //Objeto que contém as cartas válidas
    
    public BJControlador(){
        this.jogadores = new ArrayList<BJJogador>();
        this.baralho = new Baralho();
        this.baralho.insereBaralhoPadrao();         //54 cartas, 4 valores iguais sendo um de cada naipe
        this.bitsControle = new boolean[2];
        
    }

    /**
     *  Construtor que altera máximo de jogadores, valor definido pelo usuário
     * @param maxJogadores
     */
    public BJControlador(int maxJogadores){
        this.maxJogadores = maxJogadores;
        this.jogadores = new ArrayList<BJJogador>();
        this.baralho = new Baralho();
        this.baralho.insereBaralhoPadrao();
        this.bitsControle = new boolean[2];
    }
    
    private void zeraBitsControle(){
        this.bitsControle[0] = false;               //JOGANDO?
        this.bitsControle[1] = false;               //CARTAS DISTRIBUIDAS?
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
//        this.bitsControle[0] = false;
    }
    
    /**
     *
     */
    public void iniciaJogo(){
        if (jogadores.size() < 1){
            System.out.println("Erro, não há jogadores.");
            return;
        }
        BJJogador novo = new BJJogador ("Crupiê",999999);
        loopJogo();
        return;
    }
    
    /**
     *
     * @param nome
     * @param fichas
     */
    public void insereJogador(String nome,int fichas){
        if (jogadores.size() == 8){
            System.out.println("Erro, máximo de jogadores excedido.");
            return;
        }
        BJJogador novo = new BJJogador (nome,fichas);
        this.jogadores.add(novo);
    }
    
    /**
     *
     */
    public void loopJogo(){
        this.bitsControle[0] = true;
        while (this.bitsControle[0]){
            if (!this.bitsControle[1]){
                primeirasCartas();
            }
            
        }
    }
    
    /**
     *
     */
    public void primeirasCartas(){
        Carta carta = new Carta("","");
        for ( int i = 0 ; i < this.jogadores.size(); i++ ){
            
        }
    }
    
}
