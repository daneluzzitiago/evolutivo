/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BlackJack;

import Baralho.Carta;
import java.util.ArrayList;

/**
 *
 * @author tldan
 */
public class BJJogador {
    private String nome;
    private int fichas;
    private int totalCartas;
    private ArrayList<Carta> mao;
    private boolean jogando;
    
    public BJJogador(String nome, int fichas){
        this.nome = nome;
        this.fichas = fichas;
        this.totalCartas = 0;
        this.mao = new ArrayList<Carta>();
        this.jogando = false;
    }
}
