/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Baralho;

/**
 *
 * @author tldan
 */
public class Carta {
    private String numero;
    private String naipe;

    public Carta(String numero, String naipe) {
        this.numero = numero;
        this.naipe = naipe;
    }

    public String getNumero() {
        return numero;
    }

    public String getNaipe() {
        return naipe;
    }
    
    
}
