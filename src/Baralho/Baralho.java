/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Baralho;

import java.util.ArrayList;

/**
 *
 * @author tldan
 */
public class Baralho {
    private ArrayList<Carta> cartas;

    public Baralho() {
        this.cartas = new ArrayList<Carta>();
    }
    
    public void insereCarta(String c, String s){
        Carta carta = new Carta(c,s);
        this.cartas.add(carta);
    }
    
    public void removeCarta(int index){
        if (cartas.size() == 0){            //Não é possível remover de baralho vazio
            System.out.println("Erro, não há cartas");
            return;
        }
        this.cartas.remove(index);
        return;
    }
    
    /**
     * Faz uma cópia da carta e armazena em um auxiliar, removendo o objeto do Array e retornando-o
     * @param index identifica a posição da carta que sera retirada
     * @return o objeto Carta
     */
    public Carta retiraCarta(int index){
        if (index < 0 || index >= cartas.size()){
            System.out.println("Erro, index fora de alcance");
            return null;
        }
        Carta aux = new Carta(  this.cartas.get(index).getNumero(),     //Copia do ovbjeto a ser removido
                                this.cartas.get(index).getNaipe()   );
        removeCarta(index);
        return aux;
    }
    
    /**
     * Semelhante ao método acima, mas buscando se existe carta com os parâmetros enviados
     * @param c Valor da carta
     * @param s Naipe
     * @return o objeto Carta, caso existente
     */
    public Carta retiraCarta(String c, String s){
        for ( int i = 0 ; i < this.cartas.size() ; i++ ){
            if ( this.cartas.get(i).getNumero() == c && this.cartas.get(i).getNaipe() == s){
                Carta aux = new Carta (c,s);
                removeCarta(i);
                return aux;
            }
        }
        System.out.println("Erro, carta não encontrada");
        return null;
    }
    
    /**
     * Puxa uma carta aleatória do baralho a partir de um random, de uma maneira que não seja possível saber qual a próxima carta
     * @return objeto Carta do index especificado
     */
    public Carta pegaUma(){
        int index = (int) (Math.random()*cartas.size());
        return ( this.retiraCarta(index));
    }
    
    /**
     * Cria um novo ArrayList de cartas fazendo remoções aleatórias, adicionando no novo Array cópias do objeto removido
     */
    public void embaralhar(){
        if ( cartas.size() == 0 ){  //Não há o que embaralhar
            return;
        }
        int nRandom;
        int nVezes = cartas.size();
        ArrayList<Carta> novo = new ArrayList<Carta>();     //Novo Array
        for ( int i = 0 ; i < nVezes ; i++ ){
            novo.add( pegaUma() );
        }
        this.cartas = novo;
    }
    
    /**
     * 54 cartas de Ás à 10 mais Valete, Dama, Rei, todos com uma cópia de cada naipe
     */
    public void insereBaralhoPadrao(){
        this.insereCarta("Ás", "Paus");     this.insereCarta("Ás", "Copas");        this.insereCarta("Ás", "Espadas");      this.insereCarta("Ás", "Ouros");
        this.insereCarta("2", "Paus");      this.insereCarta("2", "Copas");         this.insereCarta("2", "Espadas");       this.insereCarta("2", "Ouros");
        this.insereCarta("3", "Paus");      this.insereCarta("3", "Copas");         this.insereCarta("3", "Espadas");       this.insereCarta("3", "Ouros");
        this.insereCarta("4", "Paus");      this.insereCarta("4", "Copas");         this.insereCarta("4", "Espadas");       this.insereCarta("4", "Ouros");
        this.insereCarta("5", "Paus");      this.insereCarta("5", "Copas");         this.insereCarta("5", "Espadas");       this.insereCarta("5", "Ouros");
        this.insereCarta("6", "Paus");      this.insereCarta("6", "Copas");         this.insereCarta("6", "Espadas");       this.insereCarta("6", "Ouros");
        this.insereCarta("7", "Paus");      this.insereCarta("7", "Copas");         this.insereCarta("7", "Espadas");       this.insereCarta("7", "Ouros");
        this.insereCarta("8", "Paus");      this.insereCarta("8", "Copas");         this.insereCarta("8", "Espadas");       this.insereCarta("8", "Ouros");
        this.insereCarta("9", "Paus");      this.insereCarta("9", "Copas");         this.insereCarta("9", "Espadas");       this.insereCarta("9", "Ouros");
        this.insereCarta("10", "Paus");     this.insereCarta("10", "Copas");        this.insereCarta("10", "Espadas");      this.insereCarta("10", "Ouros");
        this.insereCarta("Valete", "Paus"); this.insereCarta("Valete", "Copas");    this.insereCarta("Valete", "Espadas");  this.insereCarta("Valete", "Ouros");
        this.insereCarta("Dama", "Paus");   this.insereCarta("Dama", "Copas");      this.insereCarta("Dama", "Espadas");    this.insereCarta("Dama", "Ouros");
        this.insereCarta("Rei", "Paus");    this.insereCarta("Rei", "Copas");       this.insereCarta("Rei", "Espadas");     this.insereCarta("Rei", "Ouros"); 
    }
    
    public void exibeCartas(){
        for ( int i = 0; i < cartas.size(); i++){
            System.out.println(""+this.cartas.get(i).getNumero()+" de "+this.cartas.get(i).getNaipe()+".");
        }
    }
  
}
