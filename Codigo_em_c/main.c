#include <stdio.h>
#include <stdlib.h>
#include "baralho.h"

int main(){
	
	BARALHO *b;
	b = iniciaBaralho();
	
	//Testes;
	//printaBaralho(b);
	//insereBaralhoCompleto(b);
	
	insereBaralhoComum(b);
	//removeCarta(b,4,"paus");
	//removeCarta(b,3,"xinfornfula");
	//printaBaralho(b);
	
	liberaBaralho(b);
	
	return 0;
}
