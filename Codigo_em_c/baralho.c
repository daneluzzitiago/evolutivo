//.c
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "baralho.h"

struct baralho{
	struct carta *primeira;
	int tam;
};

typedef struct carta{
	struct carta *prox;
	int valor;
	char *naipe;
}carta;

BARALHO* iniciaBaralho(){
	BARALHO *b = (BARALHO*)malloc(sizeof(BARALHO));
	b->tam = 0;
	b->primeira = NULL;
	return b;
}

void liberaBaralho(BARALHO *b){
	free(b);
	return;
}

void insereCarta(BARALHO *b, int valor, char *naipe){
	carta *c = (carta*)malloc(sizeof(carta));
	c->valor = valor;
	c->naipe = naipe;
	c->prox == NULL;
	if(b->primeira == NULL){
		b->primeira = c;
	} else{
		carta *aux = (carta*)malloc(sizeof(carta));
		aux = b->primeira;
		int i;
		for( i = 1 ; i < b->tam; i++){
			aux = aux->prox;
		}
		aux->prox = c;	
	}
	b->tam++;	
	return;
}
//Hello

void printaBaralho(BARALHO *b){
	if(b == NULL || b->tam == 0){
		printf("Baralho vazio\n");
	}
	int i;
	carta *c = (carta*)malloc(sizeof(carta));
	c = b->primeira;
	while (c != NULL){
		if (c->valor == 74){
			printf("Valete de %s\n",c->naipe);
		} else if(c->valor == 81){
			printf("Dama de %s\n", c->naipe);
		}else if(c->valor == 75){
			printf("Rei de %s\n", c->naipe);
		}else if(c->valor == 65){
			printf("As de %s\n", c->naipe);
		}else{
			printf("%d de %s\n",c->valor,c->naipe);
		}
		c = c->prox;
	}
	return;
}

void insereBaralhoComum(BARALHO *b){
	insereCarta(b,2,"paus");insereCarta(b,2,"espadas");insereCarta(b,2,"copas");insereCarta(b,2,"ouros");
	insereCarta(b,3,"paus");insereCarta(b,3,"espadas");insereCarta(b,3,"copas");insereCarta(b,3,"ouros");
	insereCarta(b,4,"paus");insereCarta(b,4,"espadas");insereCarta(b,4,"copas");insereCarta(b,4,"ouros");
	insereCarta(b,5,"paus");insereCarta(b,5,"espadas");insereCarta(b,5,"copas");insereCarta(b,5,"ouros");
	insereCarta(b,6,"paus");insereCarta(b,6,"espadas");insereCarta(b,6,"copas");insereCarta(b,6,"ouros");
	insereCarta(b,7,"paus");insereCarta(b,7,"espadas");insereCarta(b,7,"copas");insereCarta(b,7,"ouros");
	insereCarta(b,8,"paus");insereCarta(b,8,"espadas");insereCarta(b,8,"copas");insereCarta(b,8,"ouros");
	insereCarta(b,9,"paus");insereCarta(b,9,"espadas");insereCarta(b,9,"copas");insereCarta(b,9,"ouros");
	insereCarta(b,10,"paus");insereCarta(b,10,"espadas");insereCarta(b,10,"copas");insereCarta(b,10,"ouros");
	insereCarta(b,'J',"paus");insereCarta(b,'J',"espadas");insereCarta(b,'J',"copas");insereCarta(b,'J',"ouros");
	insereCarta(b,'Q',"paus");insereCarta(b,'Q',"espadas");insereCarta(b,'Q',"copas");insereCarta(b,'Q',"ouros");
	insereCarta(b,'K',"paus");insereCarta(b,'K',"espadas");insereCarta(b,'K',"copas");insereCarta(b,'K',"ouros");
	insereCarta(b,'A',"paus");insereCarta(b,'A',"espadas");insereCarta(b,'A',"copas");insereCarta(b,'A',"ouros");
}

void removeCarta(BARALHO *b, int valor, char *naipe){
	carta *aux = (carta*)malloc(sizeof(carta));
	aux = b->primeira;
	//Caso em que a carta est� na primeira posi��o
	if (b->primeira->valor == valor && strcmp(b->primeira->naipe,naipe) == 0){
		b->primeira = b->primeira->prox;
		free(aux);	
		b->tam--;
		return;
	}else{
		int iterador = b->tam-1;
		while (iterador > 0){
			if ( aux->prox->valor == valor && strcmp( (aux->prox->naipe) , (naipe) ) == 0){ //testa se a pr�xima carta � o que procura
				//printf("%d de %s eh igual a %d de %s\n",aux->prox->valor,aux->prox->naipe,valor,naipe);
				//Caso em que a carta est� na �ltima posi��o
				if (aux->prox->prox == NULL){
					free(aux->prox);
					b->tam--;
					return;
				}else{
					//Caso em que est� entre outras duas cartas
					carta *temp = (carta*)malloc(sizeof(carta));
					temp = aux->prox;
					aux->prox = temp->prox;
					free(temp);
					b->tam--;
					return;
				}
			}
			//printf("%d de %s eh diferente de %d de %s\n",aux->prox->valor,aux->prox->naipe,valor,naipe);
			aux = aux->prox;
			iterador--;
		}
		//S� vai sair do while se a carta n�o existir. Nesse caso printa uma mensagem de erro
		printf("Carta nao encontrada\n");
		return;
	}
	

	
	
}















