#ifndef BARALHO_H
#define BARALHO_H

//.h

typedef struct baralho BARALHO;


//Retorna tipo baralho com mem�ria alocada
BARALHO* iniciaBaralho();
//Free no vetor cartas e no baralho
void liberaBaralho(BARALHO *b);
//Insere tipo carta em um baralho, definindo seus atributos atrav�s dos par�metros
void insereCarta(BARALHO *b, int valor, char *naipe);
//Acessa todos os elementos da lista e printa na tela seus atributos
void printaBaralho(BARALHO *b);
//Insere o baralho de 54 cartas, �s at� Rei dos 4 naipes
void insereBaralhoComum(BARALHO *b);
//Remove carta do baralho, passando seus atributos como par�metro
void removeCarta(BARALHO *b, int valor, char *naipe);


#endif
